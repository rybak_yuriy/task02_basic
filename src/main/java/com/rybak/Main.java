package com.rybak;

import java.util.Scanner;


/**
 * The application main class.
 * <h1>Java Basic Lesson application </h1>
 *
 * @author Yuriy Rybak
 * @version 1.0 07 Nov 2019
 */

public class Main {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */

    public static void main(String[] args) {
        int sumOfOdd = 0;
        int sumOfEven = 0;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter a start number of interval:");
        int start = scanner.nextInt();
        System.out.println("Please, enter an end number of interval:");
        int end = scanner.nextInt();

        if (start > end) {
            System.out.println("The start of interval has to be bigger than end");
        }
        /**
         * Printout odd numbers
         * Calculate a sum of odd numbers
         */
        for (int i = start; i <= end; i++) {
            if (checkIfOdd(i)) {
                System.out.println(i + " is odd");
                sumOfOdd += i;
            }
        }
        System.out.println("Sum of odd numbers is: " + sumOfOdd);


        /**
         * Printout even numbers
         * Calculate a sum of even numbers
         */
        for (int i = end; i >= start; i--) {
            if (!checkIfOdd(i)) {
                System.out.println(i + " is even");
                sumOfEven += i;
            }
        }
        System.out.println("Sum of even numbers is: " + sumOfEven);

        /**
         * Get and printout a fibonacci sequence
         * Start of sequence is the biggest odd number of entered interval
         * Second number in sequence is the biggest even number of entered interval
         */
        if (start < end) {
            System.out.println("Please, enter a size of fibonacci sequence:");
            int size = scanner.nextInt();
            int biggestOdd = 0;
            int biggestEven = 0;
            for (int i = start; i <= end; i++) {
                if (checkIfOdd(i)) {
                    if (biggestOdd < i) {
                        biggestOdd = i;
                    }
                }
                if (!checkIfOdd(i)) {
                    if (biggestEven < i) {
                        biggestEven = i;
                    }
                }
            }
            int[] fibonacciArr = FibonacciNumber.getFibonacciSequence(biggestOdd, biggestEven, size);
            for (int i = 0; i < fibonacciArr.length; i++) {
                System.out.println("Fibonacci number_" + i + "= " + fibonacciArr[i]);
            }


            /**
             * Calculate and printout a sum of the fibonacci sequence
             */
            System.out.println("Sum of the fibonacci sequence= " + FibonacciNumber.getSum(fibonacciArr));

            /**
             * Calculate and printout a percentage of odd and even number in array.
             */
            double percentageOfOdd = FibonacciNumber.calcPercentageOdd(fibonacciArr);
            System.out.println("Percentage of odd numbers= " + percentageOfOdd);
            double percentageOfEven = 100 - percentageOfOdd;
            System.out.println("Percentage of even numbers= " + percentageOfEven);
        }
    }

    /**
     * <p>Method checks a number if it is odd</p>
     *
     * @param number to check
     * @return return true if number is odd
     */
    public static boolean checkIfOdd(int number) {
        return number % 2 == 0;
    }

}
