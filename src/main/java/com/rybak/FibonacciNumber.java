package com.rybak;

/**
 * Generate a fibonacci sequence and analyze it.
 *
 * @author Yuriy Rybak
 */

public class FibonacciNumber {

    /**
     * <p>Method generate a fibonacci sequence</p>
     * Method generate a fibonacci sequence of the specified size
     *
     * @param start  is the first number of sequence
     * @param second is the second number of sequence
     * @param size   is quantity of numbers in sequence
     * @return array [] int
     */

    public static int[] getFibonacciSequence(int start, int second, int size) {
        int[] fibonacci = new int[size];
        fibonacci[0] = start;
        fibonacci[1] = second;
        for (int i = 2; i < fibonacci.length; i++) {

            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }

        return fibonacci;
    }

    /**
     * <p>Method calculate a sum of array.</p>
     *
     * @param arr to calculate
     * @return int
     */

    public static int getSum(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        return sum;
    }

    /**
     * <p>Method calculate a percentage of odd number in array.</p>
     *
     * @param arr to calculate
     * @return double
     */
    public static double calcPercentageOdd(int[] arr) {
        int numOfOdd = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                numOfOdd++;
            }
        }
        int l = arr.length;
        double percentage = (double) numOfOdd / l;

        return percentage * 100;
    }
}
